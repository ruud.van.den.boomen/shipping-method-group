import csv
import re

from django.db.utils import IntegrityError

from sendcloud.shipping_methods.models import ShippingMethod, ShippingMethodGroup, ShippingProduct


fields = ["method_1", "method_2", "differences"]

ShippingMethod.objects.all().update(group=None)
ShippingMethodGroup.objects.all().delete()
all_methods = ShippingMethod.objects.all().values_list("code", flat=True)

# Regex pattern to filter weight group in shipipng method code
weight_pattern = r"kg=\d+(\.\d+)?-\d+(\.\d+)?,?"
# Regex pattern to direct contract only in shipipng method code
dco_pattern = r",?direct_contract_only"


def create_regex(code):
    # Create a regex to find methods with a similar code
    regex = re.sub(weight_pattern, "kg=\\\\d+(\\\\.\\\\d+)?-\\\\d+(\\\\.\\\\d+)?,?", code)
    regex = re.sub(weight_pattern, "kg=\\\\d+(\\\\.\\\\d+)?,?", regex)
    if "direct_contract_only" in regex:
        regex = regex.replace(",direct_contract_only", "(,direct_contract_only)?")
    else:
        regex = f"{regex}(,direct_contract_only)?"
    regex.replace("/", r"\/")
    regex = re.compile(f"^{regex}$")
    return regex


with open("./duplicate_with_different_functionalities.csv", "w") as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=fields, quotechar='"')
    writer.writeheader()
    products = ShippingProduct.objects.filter()
    for product in products:
        unique_functionalities = [
            dict(t) for t in {tuple(d.all_functionalities.items()) for d in product.methods.all()}
        ]

        for functionalities in unique_functionalities:
            functionalities.pop("direct_contract_only")
            methods = ShippingMethod.objects.filter(
                all_functionalities__contains=functionalities, product__code=product.code
            ).order_by("min_weight")
            method = methods[0]

            group_code = re.sub(weight_pattern, "", method.code)
            group_code = re.sub(dco_pattern, "", group_code)

            max_weight = max(methods.values_list("max_weight", flat=True))
            min_weight = min(methods.values_list("min_weight", flat=True))

            regex = create_regex(method.code)

            # Filter out methods with similar code
            matches = [element for element in all_methods if regex.match(element)]

            same_code_count = ShippingMethod.objects.filter(product__code=product.code, code__in=matches, is_active=True).count()
            same_functionalities_count = methods.count()
            try:
                assert same_code_count == same_functionalities_count
            except AssertionError:
                if same_code_count > same_functionalities_count:
                    extra_method = ShippingMethod.objects.filter(code__in=matches, is_active=True).exclude(pk__in=methods).first()
                    # Let's see what the diff is in functionalities
                    set1 = set(extra_method.all_functionalities.items())
                    set2 = set(functionalities.items())
                    print(method.code)
                    print(extra_method.code)
                    diff = set1 ^ set2
                    diff = [x for x in diff if x[0] != "direct_contract_only"]
                    print(diff)
                    writer.writerow({"method_1": method.code, "method_2": extra_method.code, "differences": diff})

            try:
                group = ShippingMethodGroup.objects.create(
                    code=group_code,
                    carrier=method.carrier,
                    product=product,
                    functionalities=functionalities,
                    max_weight=max_weight,
                    min_weight=min_weight,
                    max_length=method.max_dimension_l,
                    max_width=method.max_dimension_w,
                    max_height=method.max_dimension_h,
                    max_total_size=method.max_total_size,
                )
            except IntegrityError:
                continue
            group.shipping_methods.add(*methods)
